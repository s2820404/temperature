package com.temperature.temperature;

public class Converter {

    double getF(String c) {
        return Double.parseDouble(c) * 9 / 5 + 32;
    }
}
